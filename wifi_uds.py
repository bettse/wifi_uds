#!/usr/bin/env python

import time, random, math
import sys, os, signal
from multiprocessing import Process

from liboui2 import Oui
from scapy.all import *
from telegraf.client import TelegrafClient
from ConfigParser import ConfigParser

#Limit delimiters to only '=' since hw addresses have colons
config = ConfigParser()
config.read('wifi_uds.cfg')

myoui = Oui('oui.txt')
client = TelegrafClient(host='10.0.1.200', port=8092)

def PacketHandler(pkt) :
    now = int(time.time())
    if pkt.haslayer(Dot11) :
        if pkt.addr2 is not None:
            oui = myoui.search(pkt.addr2, 'm')
            d = dbm(pkt)

            if pkt.type == 0 and pkt.subtype == 4:
                #print "[%d] %s (%s) %d" % (now, alias(pkt.addr2), oui, d)
                client.metric('dbm', d, tags={'type': 'probe_request', 'addr': pkt.addr2, 'alias': alias(pkt.addr2), 'oui': oui})

            if pkt.type == 2:
                #print "[%d] AP: %s, client: %s (%s) %d" % (now, pkt.addr3, alias(pkt.addr2), oui, d)
                client.metric('dbm', d, tags={'type': 'data', 'addr': pkt.addr2, 'alias': alias(pkt.addr2), 'bssid': pkt.addr3, 'oui': oui})

def alias(addr):
    if config.has_option('Alias', addr):
        return config.get('Alias', addr)
    return addr

def dbm(pkt):
    nd = pkt[RadioTap].notdecoded
    return float(-(256-ord(nd[-4:-3])))

# Channel hopper
def channel_hopper():
    while True:
        try:
            channel = random.choice(range(1, 14))
            #print "[+] Sniffing on channel " + str(channel)
            #os.system("iw dev %s set channel %d" % (interface, channel))
            os.system("iwconfig " + iface + " channel " + str(channel))
            time.sleep(3)
        except KeyboardInterrupt:
            break


# Capture interrupt signal and cleanup before exiting
def signal_handler(signal, frame):
    print("Cleaning up")
    p.terminate()
    p.join()
    sys.exit(0)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        iface = str(sys.argv[1])
    else:
        iface = "wlan1"

    os.system("ifconfig " + iface + " down")
    os.system("iwconfig " + iface + " mode monitor")
    os.system("ifconfig " + iface + " up")

    # Start the channel hopper
    p = Process(target = channel_hopper)
    p.start()

    # Capture CTRL-C
    signal.signal(signal.SIGINT, signal_handler)

    print "[+] Sniffing on interface " + iface + ": "
    sniff(iface=iface, prn=PacketHandler, store=0)

