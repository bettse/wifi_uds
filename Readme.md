# Wifi User Detection System

Inspired by: https://www.reddit.com/r/raspberry_pi/comments/3ynxp9/wifispi_my_wardriving_raspberry_pi_setup/cyf64wy

Intended to run on a Raspberry Pi, using with an instance of Telegraf, and Influxdb running on a remote host.

install https://github.com/John-Lin/py80211.git

https://pypi.io/project/configparser/#files
